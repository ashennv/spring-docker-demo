package com.zxc.springdockerdemo;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloService {

    @GetMapping("/status")
    public ResponseEntity<String> analyze() {
        return ResponseEntity.ok().body(" Service is Working");
    }


}
